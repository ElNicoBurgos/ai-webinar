// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WebinarPatrolComponent.generated.h"


class ATargetPoint;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WEBINARAI_API UWebinarPatrolComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	/** Retrieve patrol points. */
	TArray<ATargetPoint*>GetPatrolPoints() const { return PatrolPoints; }

private:

	UPROPERTY(EditInstanceOnly, meta = (AllowPrivateAccess = true))
	TArray<ATargetPoint*> PatrolPoints;
	
};
