// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTT_Patrol.generated.h"

/**
 * We use this task to select a location point for the IA to go to.
 */
UCLASS()
class WEBINARAI_API UBTT_Patrol : public UBTTaskNode
{
	GENERATED_BODY()

public:

	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:

	/** This variable we use to store the new location. */
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector TargetPoint;
};
