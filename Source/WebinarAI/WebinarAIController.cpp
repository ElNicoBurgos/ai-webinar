// Fill out your copyright notice in the Description page of Project Settings.

#include "WebinarAIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"

AWebinarAIController::AWebinarAIController()
{
	AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));
	SightSenseConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightSenseConfig"));

	SightSenseConfig->SightRadius = 1000.0f;
	SightSenseConfig->LoseSightRadius = 2000.0f;
	SightSenseConfig->PeripheralVisionAngleDegrees = 10.0f;
	SightSenseConfig->SetMaxAge(5.0f);
	SightSenseConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightSenseConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightSenseConfig->DetectionByAffiliation.bDetectNeutrals = true;

	// Add sight configuration component to perception component
	AIPerceptionComponent->SetDominantSense(*SightSenseConfig->GetSenseImplementation());
	AIPerceptionComponent->ConfigureSense(*SightSenseConfig);
}

void AWebinarAIController::OnPossess(APawn* PawnPossess)
{
	Super::OnPossess(PawnPossess);
	check(BaseTreeToRun);
	RunBehaviorTree(BaseTreeToRun);
}

void AWebinarAIController::OnPerceptionUpdate(AActor* Actor, FAIStimulus Stimulus)
{
	if (PerceptionComponent->GetSenseConfig(Stimulus.Type) == SightSenseConfig)
	{
		if (Actor)
		{
			GetBlackboardComponent()->SetValueAsObject(FName(TEXT("TargetPlayer")), Actor);
		}
	}
}

void AWebinarAIController::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	AIPerceptionComponent->OnTargetPerceptionUpdated.AddUniqueDynamic(this, &AWebinarAIController::OnPerceptionUpdate);
}