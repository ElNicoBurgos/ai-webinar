// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "WebinarAIController.generated.h"

class UAIPerceptionComponent;
class UAISenseConfig_Sight;

/**
 * 
 */
UCLASS()
class WEBINARAI_API AWebinarAIController : public AAIController
{
	GENERATED_BODY()

public:

	AWebinarAIController();

	/** Called when the AI is posses */
	void OnPossess(APawn* PawnPossess) override;

protected:

	/** Allow actors to initialize themselves on the C++ side after all of their components have been initialized. */
	void PostInitializeComponents() override;

	/** Which behavior tree we should run. */
	UPROPERTY(EditDefaultsOnly, Category = "AI|BehaviorTree")
	UBehaviorTree* BaseTreeToRun;
	
private:

	/** Call back when the AI see a possible target. */
	UFUNCTION()
	void OnPerceptionUpdate(AActor* Actor, FAIStimulus Stimulus);

	UPROPERTY(VisibleAnywhere, Category = "AI|Perception")
	UAIPerceptionComponent* AIPerceptionComponent;

	UPROPERTY()
	UAISenseConfig_Sight* SightSenseConfig;
};