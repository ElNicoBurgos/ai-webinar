// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_Patrol.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Engine/TargetPoint.h"
#include "GameFramework/Character.h"
#include "WebinarPatrolComponent.h"

EBTNodeResult::Type UBTT_Patrol::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	EBTNodeResult::Type TaskResult = EBTNodeResult::Failed;

	const ACharacter* AI = OwnerComp.GetAIOwner()->GetCharacter();
	check(AI);
	
	const UWebinarPatrolComponent* PatrolComponent = AI->FindComponentByClass<UWebinarPatrolComponent>();
	if(!PatrolComponent)
	{
		return TaskResult;
	}
	
	TArray<ATargetPoint*> TargetPoints = PatrolComponent->GetPatrolPoints();

	if(TargetPoints.Num() > 0)
	{
		const int32 TargetPointIndex = FMath::RandRange(0, TargetPoints.Num() - 1);
		const FVector& NewTargetPointLocation = TargetPoints[TargetPointIndex]->GetActorLocation();
		
		UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
		const FVector& PreviousTargetPointLocation = BlackboardComp->GetValueAsVector(TargetPoint.SelectedKeyName);

		if(NewTargetPointLocation != PreviousTargetPointLocation)
		{
			BlackboardComp->SetValueAsVector(TargetPoint.SelectedKeyName, TargetPoints[TargetPointIndex]->GetActorLocation());
			TaskResult = EBTNodeResult::Succeeded;
		}
	}

	return TaskResult;
}
