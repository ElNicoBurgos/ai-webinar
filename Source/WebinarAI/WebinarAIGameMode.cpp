// Copyright Epic Games, Inc. All Rights Reserved.

#include "WebinarAIGameMode.h"
#include "WebinarAICharacter.h"
#include "UObject/ConstructorHelpers.h"

AWebinarAIGameMode::AWebinarAIGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
