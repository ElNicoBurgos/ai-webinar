// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "WebinarAIGameMode.generated.h"

UCLASS(minimalapi)
class AWebinarAIGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWebinarAIGameMode();
};



